import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  cityNames:string[] = ['Jerusalem','London','Paris','non-existent'];
  temperature:number;
  city:string; 

  constructor(private router: Router) { }

  onSubmit(){
    this.router.navigate(['/temperatures', this.city]);
  }

  ngOnInit() {
  }

}
