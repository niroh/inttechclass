import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>

  constructor(public afAuth:AngularFireAuth,
              private router:Router) {
    this.user = this.afAuth.authState;
  }
  
  signup(email:string , password:string){
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(user => {
        this.router.navigate (['/books'])
      })
  }

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(user => {
      this.router.navigate (['/books'])
    })
  }
  
  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Yahuuu', res));
  }


}
