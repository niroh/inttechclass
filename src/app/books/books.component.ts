import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import {BooksService} from './../books.service'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  books: any;
  books$:Observable<any>;
  userId:string

  deleteBook(id:string){
    this.booksservice.deleteBook(id, this.userId);
  }

  constructor(private booksservice:BooksService,
              public authservice:AuthService) { }

  ngOnInit() {
    /*
    this.books = this.booksservice.getBooks().subscribe(
      (books) => this.books = books
    )
    
   //this.booksservice.addBooks();
    this.books$ = this.booksservice.getBooks();
    */
   this.authservice.user.subscribe(
     user => {
       this.userId = user.uid;
       this.books$ = this.booksservice.getBooks(this.userId);
     }
   )
  }

}
