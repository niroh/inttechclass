import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';
import { Observable } from 'rxjs';
import { TempService } from '../temp.service';


@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  likes:number = 0;

  temperature:number;
  city:String;
  tempData$: Observable<Weather>;
  image:String;
  errorMessage:String;
  hasError:Boolean = false;
  
  addLikes(){
    this.likes ++ //likes that belongs to this section...
  }

  constructor(private route: ActivatedRoute, private tempService:TempService) { }

 

  ngOnInit() {
      
    this.city = this.route.snapshot.params.city;
    this.tempData$ = this.tempService.searchWeatherData(this.city);
    this.tempData$.subscribe(
      data=> {
        console.log(data);
        this.temperature = data.temperature;
        this.image=data.image;
    },
    error =>{
      this.hasError = true;
      this.errorMessage = error.message;
      console.log("in the component " + error.message);
    }
    )
}



}
