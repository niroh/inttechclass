// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAjjDmLOJ2R77YMr_LNwHD16W4wID02Zng",
    authDomain: "inttech2019-b99a9.firebaseapp.com",
    databaseURL: "https://inttech2019-b99a9.firebaseio.com",
    projectId: "inttech2019-b99a9",
    storageBucket: "inttech2019-b99a9.appspot.com",
    messagingSenderId: "694664551941",
    appId: "1:694664551941:web:b28b8e4ceb7bc1ae95066c"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
